package database;

import java.io.Serializable;

public class Contacto implements Serializable {
    private  int id;
    private String nnombre1;
    private  String telefono;
    private String telefono2;
    private String domicilio;
    private String notas;
    private int favorito;

    public Contacto(int id, String nnombre1, String telefono, String telefono2, String domicilio, String notas, int favorito) {
        this.id = id;
        this.nnombre1 = nnombre1;
        this.telefono = telefono;
        this.telefono2 = telefono2;
        this.domicilio = domicilio;
        this.notas = notas;
        this.favorito = favorito;
    }

    public Contacto() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNnombre1() {
        return nnombre1;
    }

    public void setNnombre1(String nnombre1) {
        this.nnombre1 = nnombre1;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }
}
